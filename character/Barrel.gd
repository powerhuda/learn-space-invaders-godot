extends Node2D
@export var laserScene: PackedScene

var can_player_shoot = true

func _input(event):
	if Input.is_action_just_pressed("shoot") && can_player_shoot:
		can_player_shoot = false
		var laser = laserScene.instantiate() as Laser
		laser.global_position = get_parent().global_position - Vector2(0, 25)
		get_tree().root.get_node("Level1").add_child(laser)
		laser.tree_exited.connect(on_laser_destroyed)

func on_laser_destroyed():
	can_player_shoot = true
