extends Area2D

@export var speed: float = 200.0
@export var position_y: float = 10.0
@onready var collision_rect: CollisionShape2D = $CollisionShape2D
var direction = Vector2.ZERO

var bounding_size_x # digunakan untuk mendapatkan ukuran viewport
var start_bound #digunakan untuk menentukan nilai maksimal ujung kiri
var end_bound #digunakan untuk menentukan nilai maksimal ujung kanan

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	bounding_size_x = get_viewport().get_visible_rect().size.x
	
	var rect = get_viewport().get_visible_rect()
	var camera = get_viewport().get_camera_2d()
	var camera_position = camera.position
	start_bound = (camera_position.x - rect.size.x) / 2
	end_bound = (camera_position.x + rect.size.x) / 2
	#print("start bound = ",start_bound)
	#print("end bound = ",end_bound)
	#print("rect area =",rect)
	#print("rect area =",(rect.size.y/2))
	position.y = (rect.size.y/2) - 40
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	var input = Input.get_axis("left","right")
	#position.y = position_y
	if input > 0:
		direction = Vector2.RIGHT
	elif input < 0 :
		direction = Vector2.LEFT
	else:
		direction = Vector2.ZERO
		
	var delta_movement = speed * delta * direction.x
	
	var left_boundaries = start_bound + bounding_size_x
	var right_boundaries = end_bound - bounding_size_x
	#print("bounding x = ",bounding_size_x)
	#print("transform x = ",transform.get_scale().x)
	#print("left boundaries =",left_boundaries)
	#print("right boundaries =",right_boundaries)
		
	position.x += delta_movement
	#print("position x =",position.x)
