extends Node2D

class_name InvaderSpawner

#config
const ROWS = 5
const COLUMNS = 11
const HORIZONTAL_SPACING = 32
const VERTICAL_SPACING = 32
const INVADER_HEIGHT = 24
const START_Y_POSITION = -50
const INVADER_POSITION_X_INCREMENT = 10
const INVADER_POSITION_Y_INCREMENT = 20

var MOVEMENT_DIRECTION = 1
var invader_scene = preload("res://character/invader.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var invader_green = preload("res://resources/invaders/invader_green.tres")
	var invader_yellow = preload("res://resources/invaders/invader_yellow.tres")
	var invader_red = preload("res://resources/invaders/invader_red.tres")
	
	var invader_config
	
	for ROW in ROWS:
		if ROW == 0 || ROW == 1:
			invader_config = invader_green
		elif ROW == 2 || ROW == 3:
			invader_config = invader_yellow
		elif ROW == 4 || ROW == 5:
			invader_config = invader_red
			
		var row_width = (COLUMNS * invader_config.width * 3) + ((COLUMNS - 1) * HORIZONTAL_SPACING)
		var start_x = (position.x - row_width) / 2
		
		for col in COLUMNS:
			var x = start_x + (col * invader_config.widht * 3) + (col * HORIZONTAL_SPACING)
			var y = START_Y_POSITION + (ROW * INVADER_HEIGHT) + (ROW + VERTICAL_SPACING)
			var spawn_position = Vector2(x, y)
			spawn_invader(invader_config, spawn_position)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func spwan_invader(invader_config, spawn_position:Vector2):
	var invader = invader_scene.instantiate() as InvaderSpawner
