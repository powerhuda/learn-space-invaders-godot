extends CanvasLayer
@onready var texture_green: TextureRect = %TextureGreen
@onready var label_green: Label = %LabelGreen
@onready var texture_yellow: TextureRect = %TextureYellow
@onready var label_yellow: Label = %LabelYellow
@onready var texture_red: TextureRect = %TextureRed
@onready var label_red: Label = %LabelRed
@onready var timer: Timer = $Timer

var control_array = []
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	control_array.append_array([texture_green, label_green, texture_yellow, label_yellow, texture_red, label_red])
	for control in control_array:
		(control as Control).visible = false




func load_game() -> void:
	get_tree().change_scene_to_file("res://level/level_1.tscn")


func _on_timer_timeout() -> void:
	var control = control_array.pop_front() as Control
	if control != null:
		control.visible = true
	else:
		timer.stop()
		timer.queue_free()  
